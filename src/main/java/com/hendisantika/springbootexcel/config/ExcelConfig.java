package com.hendisantika.springbootexcel.config;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-excel
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/02/18
 * Time: 23.34
 * To change this template use File | Settings | File Templates.
 */
public class ExcelConfig {
    public static final String FILE_NAME = "fileName";
    public static final String HEAD = "head";
    public static final String BODY = "body";

    public static final String XLS = "xls";
    public static final String XLSX = "xlsx";
}
