package com.hendisantika.springbootexcel.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.apache.poi.ss.usermodel.Row;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-excel
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/02/18
 * Time: 23.30
 * To change this template use File | Settings | File Templates.
 */

@Data
@Builder
@AllArgsConstructor
public class Product implements Serializable {

    private String uniqueId;

    private String name;

    private String comment;

    public static Product rowOf(Row row) {
        return Product.builder()
                .uniqueId(row.getCell(0).getStringCellValue())
                .name(row.getCell(1).getStringCellValue())
                .comment(row.getCell(2).getStringCellValue())
                .build();
    }
}