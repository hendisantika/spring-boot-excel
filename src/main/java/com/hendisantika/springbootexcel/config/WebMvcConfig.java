package com.hendisantika.springbootexcel.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-excel
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 28/02/18
 * Time: 22.31
 * To change this template use File | Settings | File Templates.
 */

@Configuration
@EnableWebMvc
public class WebMvcConfig {
}
