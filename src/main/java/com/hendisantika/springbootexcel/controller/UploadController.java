package com.hendisantika.springbootexcel.controller;

import com.hendisantika.springbootexcel.component.ExcelReadComponent;
import com.hendisantika.springbootexcel.domain.Product;
import lombok.RequiredArgsConstructor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-excel
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 28/02/18
 * Time: 22.36
 * To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("upload")
@RequiredArgsConstructor
public class UploadController {
    private final ExcelReadComponent excelReadComponent;

    @PostMapping("excel")
    public List<Product> readExcel(@RequestParam("file") MultipartFile multipartFile)
            throws IOException, InvalidFormatException {
        return excelReadComponent.readExcelToList(multipartFile, Product::rowOf);
    }
}
