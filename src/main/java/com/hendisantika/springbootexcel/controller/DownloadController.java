package com.hendisantika.springbootexcel.controller;

import com.hendisantika.springbootexcel.config.ExcelConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-excel
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 28/02/18
 * Time: 22.33
 * To change this template use File | Settings | File Templates.
 */

@Controller
@RequestMapping("download")
public class DownloadController {
    @GetMapping("excel-xls")
    public ModelAndView xlsView() {
        return new ModelAndView("excelXlsView", getDefaultMap());
    }

    @GetMapping("excel-xlsx")
    public ModelAndView xlsxView() {
        return new ModelAndView("excelXlsxView", getDefaultMap());
    }

    @GetMapping("excel-xlsx-streaming")
    public ModelAndView xlsxStreamingView() {
        return new ModelAndView("excelXlsxStreamingView", getDefaultMap());
    }

    private Map<String, Object> getDefaultMap() {
        Map<String, Object> map = new HashMap<>();
        map.put(ExcelConfig.FILE_NAME, "default_excel");
        map.put(ExcelConfig.HEAD, Arrays.asList("ID", "NAME", "COMMENT"));
        map.put(ExcelConfig.BODY,
                Arrays.asList(
                        Arrays.asList("A", "a", "가"),
                        Arrays.asList("B", "b", "나"),
                        Arrays.asList("C", "c", "다")
                ));
        return map;
    }
}
